package com.marcowillemart.grpc.server;

import io.grpc.services.HealthStatusManager;
import static org.assertj.core.api.Assertions.*;
import org.junit.Test;
import org.springframework.boot.autoconfigure.AutoConfigurations;
import org.springframework.boot.test.context.runner.ApplicationContextRunner;

public class GrpcServerAutoConfigurationTest {

    private final ApplicationContextRunner contextRunner =
            new ApplicationContextRunner()
                    .withConfiguration(
                            AutoConfigurations.of(
                                    GrpcServerAutoConfiguration.class));

    @Test
    public void testDefaultAutoConfiguration() {
        contextRunner.run(context -> {
            assertThat(context).hasSingleBean(HealthStatusManager.class);
            assertThat(context).hasSingleBean(GrpcServerFactory.class);
            assertThat(context).hasSingleBean(GrpcServerLifecycle.class);
        });
    }

    @Test
    public void testCustomPort() {
        contextRunner.withPropertyValues("grpc.server.port=1234")
                .run(context -> {
                    assertThat(context).hasSingleBean(
                            GrpcServerProperties.class);
                    assertThat(context.getBean(GrpcServerProperties.class)
                            .getPort()).isEqualTo(1234);
                });
    }
}
