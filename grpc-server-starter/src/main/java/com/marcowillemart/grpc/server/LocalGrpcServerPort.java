package com.marcowillemart.grpc.server;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.beans.factory.annotation.Value;

/**
 * Annotation at the field or method/constructor parameter level that injects
 * the gRPC port that got allocated at runtime. Provides a convenient
 * alternative for @Value("${local.grpc.server.port}")
 *
 * @author Marco Willemart
 */
@Target({
    ElementType.FIELD,
    ElementType.METHOD,
    ElementType.PARAMETER,
    ElementType.ANNOTATION_TYPE
})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Value("${local.grpc.server.port}")
public @interface LocalGrpcServerPort {
}
