package com.marcowillemart.grpc.server;

import com.marcowillemart.common.util.Assert;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties for gRPC server.
 *
 * @specfield port : int               // Server port to listen on.
 * @specfield certChain : String       // Resource location of the SSL
 *                                        certificate chain.
 * @specfield privateKey : String      // Resource location of the server
 *                                        private key.
 * @specfield clientCertChain : String // Resource location of the client SSL
 *                                        certificate chain.
 *
 * @invariant port >= 0
 * @invariant RESOURCE(certChain)
 * @invariant RESOURCE(privateKey)
 * @invariant RESOURCE(clientCertChain)
 *
 * RESOURCE(r): r starts with either 'classpath:' or 'file:'
 *
 * @author Marco Willemart
 */
@ConfigurationProperties(prefix = "grpc.server")
public class GrpcServerProperties {

    private int port;
    private String certChain;
    private String privateKey;
    private String clientCertChain;

    private void checkRep() {
        Assert.isTrue(port >= 0);
        checkResourceLocation(certChain);
        checkResourceLocation(privateKey);
        checkResourceLocation(clientCertChain);
    }

    private static void checkResourceLocation(String resourceLocation) {
        Assert.notNull(resourceLocation);
        Assert.isTrue(
                resourceLocation.startsWith("classpath:")
                        || resourceLocation.startsWith("file:"));
    }

    /**
     * @effects Makes this be a new GrpcServerProperties p with
     *          p.port = 6565,
     *          p.certChain = classpath:sslcert/server.crt,
     *          p.privateKey = classpath:sslcert/server.pem,
     *          p.clientCertChain = classpath:sslcert/client.crt
     */
    public GrpcServerProperties() {
        this.port = 6565;
        this.certChain = classpathOf("server.crt");
        this.privateKey = classpathOf("server.pem");
        this.clientCertChain = classpathOf("client.crt");
    }

    /**
     * @return this.port
     */
    public int getPort() {
        return port;
    }

    /**
     * @return this.certChain
     */
    public String getCertChain() {
        return certChain;
    }

    /**
     * @return this.privateKey
     */
    public String getPrivateKey() {
        return privateKey;
    }

    /**
     * @return this.clientCertChain
     */
    public String getClientCertChain() {
        return clientCertChain;
    }

    /**
     * @requires port >= 0
     * @modifies this
     * @effects Sets this.port to port
     */
    public void setPort(int port) {
        this.port = port;

        checkRep();
    }

    /**
     * @requires certChain != null && RESOURCE(certChain)
     * @modifies this
     * @effects Sets this.certChain to certChain
     */
    public void setCertChain(String certChain) {
        this.certChain = certChain;

        checkRep();
    }

    /**
     * @requires privateKey != null && RESOURCE(privateKey)
     * @modifies this
     * @effects Sets this.privateKey to privateKey
     */
    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;

        checkRep();
    }

    /**
     * @requires clientCertChain != null && RESOURCE(clientCertChain)
     * @modifies this
     * @effects Sets this.clientCertChain to clientCertChain
     */
    public void setClientCertChain(String clientCertChain) {
        this.clientCertChain = clientCertChain;

        checkRep();
    }

    ////////////////////
    // HELPER METHODS
    ////////////////////

    private static String classpathOf(String fileName) {
        return String.format("classpath:sslcert/%s", fileName);
    }
}
