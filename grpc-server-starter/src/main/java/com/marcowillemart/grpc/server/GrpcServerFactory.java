package com.marcowillemart.grpc.server;

import io.grpc.Server;

/**
 * Creates a gRPC server with all the gRPC services added to the registry.
 * The server returned is ready to be started by the caller.
 *
 * @author Marco Willemart
 */
public interface GrpcServerFactory {

    /**
     * @return a new server
     */
    Server createServer();

    /**
     * @return the port number the server is listening on
     */
    int port();
}
