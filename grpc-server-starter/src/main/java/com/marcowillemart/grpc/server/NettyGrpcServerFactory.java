package com.marcowillemart.grpc.server;

import com.marcowillemart.common.util.Assert;
import com.marcowillemart.common.util.Resources;
import io.grpc.BindableService;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.ServerServiceDefinition;
import io.grpc.health.v1.HealthCheckResponse;
import io.grpc.netty.GrpcSslContexts;
import io.grpc.netty.NettyServerBuilder;
import io.grpc.services.HealthStatusManager;
import io.netty.handler.ssl.ClientAuth;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import java.io.File;
import java.util.List;
import javax.net.ssl.SSLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ResourceLoader;

/**
 * Factory that creates a Netty gRPC server.
 *
 * @author Marco Willemart
 */
public class NettyGrpcServerFactory implements GrpcServerFactory {

    private static final Logger LOG =
            LoggerFactory.getLogger(NettyGrpcServerFactory.class);

    private final ResourceLoader resourceLoader;
    private final GrpcServerProperties properties;
    private final HealthStatusManager healthStatusManager;
    private final List<BindableService> bindableServices;

    public NettyGrpcServerFactory(
            ResourceLoader resourceLoader,
            GrpcServerProperties properties,
            HealthStatusManager healthStatusManager,
            List<BindableService> bindableServices) {

        Assert.notNull(resourceLoader);
        Assert.notNull(properties);
        Assert.notNull(healthStatusManager);
        Assert.notNull(bindableServices);

        this.resourceLoader = resourceLoader;
        this.properties = properties;
        this.healthStatusManager = healthStatusManager;
        this.bindableServices = bindableServices;
    }

    @Override
    public Server createServer() {
        NettyServerBuilder serverBuilder =
                NettyServerBuilder.forPort(properties.getPort())
                        .sslContext(mutualAuthentication());

        serverBuilder.addService(healthStatusManager.getHealthService());

        bindableServices.forEach(bindableService ->
                register(serverBuilder, bindableService));

        return serverBuilder.build();
    }

    @Override
    public int port() {
        return properties.getPort();
    }

    ////////////////////
    // HELPER METHODS
    ////////////////////

    private void register(
            ServerBuilder<?> serverBuilder,
            BindableService bindableService) {

        ServerServiceDefinition serviceDefinition =
                bindableService.bindService();

        serverBuilder.addService(serviceDefinition);

        String serviceName = serviceDefinition.getServiceDescriptor().getName();

        healthStatusManager.setStatus(
                serviceName,
                HealthCheckResponse.ServingStatus.SERVING);

        LOG.info("'{}' service has been registered", serviceName);
    }

    private SslContext mutualAuthentication() {
        File certChainFile = toFile(properties.getCertChain());
        File privateKeyFile = toFile(properties.getPrivateKey());
        File clientCertChainFile = toFile(properties.getClientCertChain());

        SslContextBuilder sslContextBuilder =
                GrpcSslContexts.forServer(certChainFile, privateKeyFile)
                        .trustManager(clientCertChainFile)
                        .clientAuth(ClientAuth.REQUIRE);

        try {
            return sslContextBuilder.build();
        } catch (SSLException ex) {
            throw new IllegalStateException(ex.getMessage(), ex);
        }
    }

    private File toFile(String resourceLocation) {
        return Resources.toFile(resourceLoader.getResource(resourceLocation));
    }
}
