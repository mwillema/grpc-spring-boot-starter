package com.marcowillemart.grpc.server;

import io.grpc.BindableService;
import io.grpc.Server;
import io.grpc.services.HealthStatusManager;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ResourceLoader;

/**
 * AutoConfiguration for gRPC server.
 *
 * @author Marco Willemart
 */
@Configuration
@EnableConfigurationProperties(GrpcServerProperties.class)
@ConditionalOnClass({Server.class, GrpcServerFactory.class})
public class GrpcServerAutoConfiguration {

    public GrpcServerAutoConfiguration() {
    }

    @Bean
    @ConditionalOnMissingBean
    public HealthStatusManager healthStatusManager() {
        return new HealthStatusManager();
    }

    @Bean
    @ConditionalOnMissingBean
    public GrpcServerFactory grpcServerFactory(
            ResourceLoader resourceLoader,
            GrpcServerProperties properties,
            HealthStatusManager healthStatusManager,
            Optional<List<BindableService>> bindableServices) {

        return new NettyGrpcServerFactory(
                resourceLoader,
                properties,
                healthStatusManager,
                bindableServices.orElse(Collections.emptyList()));
    }

    @Bean
    @ConditionalOnMissingBean
    public GrpcServerLifecycle grpcServerLifecycle(
            ApplicationEventPublisher publisher,
            HealthStatusManager healthStatusManager,
            GrpcServerFactory factory) {

        return new GrpcServerLifecycle(publisher, healthStatusManager, factory);
    }
}
