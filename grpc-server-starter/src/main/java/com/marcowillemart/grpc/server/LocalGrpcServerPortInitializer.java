package com.marcowillemart.grpc.server;

import java.util.HashMap;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.PropertySource;

/**
 * Initializes the local port of a gRPC server when started.
 *
 * The local port of the gRPC server can be accessed through the
 * local.grpc.server.port property.
 *
 * @author Marco Willemart
 */
public class LocalGrpcServerPortInitializer
        implements ApplicationContextInitializer
        <ConfigurableApplicationContext> {

    /**
     * @effects Makes this be a new gRPC server port info application context
     *          initializer.
     */
    public LocalGrpcServerPortInitializer() {
    }

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        applicationContext.addApplicationListener(
                (GrpcServerStarted event) ->
                        onApplicationEvent(applicationContext, event));
    }

    ////////////////////
    // HELPER METHODS
    ////////////////////

    private void onApplicationEvent(
            ConfigurableApplicationContext applicationContext,
            GrpcServerStarted event) {

        MutablePropertySources sources =
                applicationContext.getEnvironment().getPropertySources();

        PropertySource<?> source = sources.get("server.ports");

        if (source == null) {
            source = new MapPropertySource("server.ports", new HashMap<>());
            sources.addFirst(source);
        }

        ((MapPropertySource) source).getSource()
                .put("local.grpc.server.port", event.port());
    }
}
