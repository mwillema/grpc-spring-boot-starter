package com.marcowillemart.grpc.server;

import io.grpc.Server;
import org.springframework.context.ApplicationEvent;

/**
 * Application event that is published when a gRPC server is started.
 *
 * @author Marco Willemart
 */
public final class GrpcServerStarted extends ApplicationEvent {

    /**
     * @requires source != null
     * @effects Makes this be a new GrpcServerStarted event for the given server
     */
    public GrpcServerStarted(Server source) {
        super(source);
    }

    /**
     * @return the port number the server is listening on
     */
    public int port() {
        return ((Server) getSource()).getPort();
    }
}
