package com.marcowillemart.grpc.server;

import com.marcowillemart.common.util.Assert;
import io.grpc.Server;
import io.grpc.ServerServiceDefinition;
import io.grpc.services.HealthStatusManager;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationEventPublisher;

/**
 * Manages the lifecycle of a gRPC server.
 *
 * It uses the {@link GrpcServerFactory} to create a new instance of a gRPC
 * server, and then manages it according to the {@link ApplicationRunner} and
 * {@link DisposableBean}.
 *
 * @author Marco Willemart
 */
public class GrpcServerLifecycle implements ApplicationRunner, DisposableBean {

    private static final Logger LOG =
            LoggerFactory.getLogger(GrpcServerLifecycle.class);

    private final ApplicationEventPublisher publisher;
    private final HealthStatusManager healthStatusManager;
    private final GrpcServerFactory factory;
    private Server server;

    public GrpcServerLifecycle(
            ApplicationEventPublisher publisher,
            HealthStatusManager healthStatusManager,
            GrpcServerFactory factory) {

        Assert.notNull(publisher);
        Assert.notNull(healthStatusManager);
        Assert.notNull(factory);

        this.publisher = publisher;
        this.healthStatusManager = healthStatusManager;
        this.factory = factory;
        this.server = null;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        LOG.info("Starting gRPC server...");

        server = factory.createServer();

        server.start();

        publisher.publishEvent(new GrpcServerStarted(server));

        LOG.info("gRPC server started on port {}", server.getPort());

        startDaemonAwaitThread();
    }

    @Override
    public void destroy() throws Exception {
        LOG.info("Shutting down gRPC server...");

        if (server == null) {
            LOG.info("gRPC server not running");
        } else {
            clearStatus(server.getServices());
            server.shutdown();
            server = null;
            LOG.info("gRPC server stopped");
        }
    }

    ////////////////////
    // HELPER METHODS
    ////////////////////

    private void startDaemonAwaitThread() {
        Thread awaitThread = new Thread(() -> {
            try {
                server.awaitTermination();
            } catch (InterruptedException ex) {
                LOG.error("gRPC server stopped", ex);
            }
        });

        awaitThread.setDaemon(false);
        awaitThread.start();
    }

    private void clearStatus(List<ServerServiceDefinition> serviceDefinitions) {
        serviceDefinitions.forEach(
                serviceDefinition ->
                        healthStatusManager.clearStatus(
                                serviceDefinition
                                        .getServiceDescriptor()
                                        .getName()));
    }
}
