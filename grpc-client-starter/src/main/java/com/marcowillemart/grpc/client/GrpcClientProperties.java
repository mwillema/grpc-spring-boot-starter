package com.marcowillemart.grpc.client;

import com.marcowillemart.common.util.Assert;
import java.util.LinkedHashMap;
import java.util.Map;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties for gRPC client.
 *
 * @specfield channels : Map<String, ChannelConfiguration> // A mapping between
 *     the name and the gRPC channel configurations.
 *
 * @author Marco Willemart
 */
@ConfigurationProperties("grpc.client")
public class GrpcClientProperties {

    public static final String DEFAULT_CHANNEL = "default";

    private final Map<String, ChannelConfiguration> channels;

    /**
     * @effects Makes this be an new GrpcClientProperties p with
     *          p.channels = {("default", new ChannelConfiguration)}
     */
    public GrpcClientProperties() {
        this.channels = new LinkedHashMap<>();
        this.channels.put(DEFAULT_CHANNEL, new ChannelConfiguration());
    }

    @SuppressWarnings("ReturnOfCollectionOrArrayField")
    public Map<String, ChannelConfiguration> getChannels() {
        return channels;
    }

    @Override
    public String toString() {
        return "GrpcClientProperties {" + "channels=" + channels + '}';
    }

    /**
     * Properties for gRPC channel configuration.
     *
     * @specfield host : String                // Host of the server.
     * @specfield port : int                   // Server port to listen on.
     * @specfield trustCertCollection : String // Resource location of the
     *                                            trusted certificates.
     * @specfield clientCertChain : String     // Resource location of the
     *                                            client SSL certificate chain.
     * @specfield clientPrivateKey : String    // Resource location of the
     *                                            client private key.
     *
     * @invariant host not empty
     * @invariant port > 0
     * @invariant RESOURCE(trustCertCollection)
     * @invariant RESOURCE(clientCertChain)
     * @invariant RESOURCE(clientPrivateKey)
     *
     * RESOURCE(r): r starts with either 'classpath:' or 'file:'
     */
    @SuppressWarnings("PublicInnerClass")
    public static class ChannelConfiguration {

        private String host;
        private int port;
        private String trustCertCollection;
        private String clientCertChain;
        private String clientPrivateKey;

        private void checkRep() {
            Assert.notEmpty(host);
            Assert.isTrue(port > 0);
            checkResourceLocation(trustCertCollection);
            checkResourceLocation(clientCertChain);
            checkResourceLocation(clientPrivateKey);
        }

        private static void checkResourceLocation(String resourceLocation) {
            Assert.notNull(resourceLocation);
            Assert.isTrue(
                resourceLocation.startsWith("classpath:")
                        || resourceLocation.startsWith("file:"));
        }

        /**
         * @effects Makes this be a new ChannelConfiguration c with
         *          c.host = localhost,
         *          c.port = 6565,
         *          c.trustCertCollection = classpath:ca.crt,
         *          c.clientCertChain = classpath:sslcert/client.crt,
         *          c.clientPrivateKey = classpath:sslcert/client.pem
         */
        public ChannelConfiguration() {
            this.host = "localhost";
            this.port = 6565;
            this.trustCertCollection = classpathOf("ca.crt");
            this.clientCertChain = classpathOf("client.crt");
            this.clientPrivateKey = classpathOf("client.pem");

            checkRep();
        }

        /**
         * @return this.host
         */
        public String getHost() {
            return host;
        }

        /**
         * @return this.port
         */
        public int getPort() {
            return port;
        }

        /**
         * @return this.trustCertCollection
         */
        public String getTrustCertCollection() {
            return trustCertCollection;
        }

        /**
         * @return this.clientCertChain
         */
        public String getClientCertChain() {
            return clientCertChain;
        }

        /**
         * @return this.clientPrivateKey
         */
        public String getClientPrivateKey() {
            return clientPrivateKey;
        }

        /**
         * @requires host not empty
         * @modifies this
         * @effects Sets this.host to host
         */
        public void setHost(String host) {
            this.host = host;

            checkRep();
        }

        /**
         * @requires port > 0
         * @modifies this
         * @effects Sets this.port to port
         */
        public void setPort(int port) {
            this.port = port;

            checkRep();
        }

        /**
         * @requires trustCertCollection not null &&
         *           RESOURCE(trustCertCollection)
         * @modifies this
         * @effects Sets this.trustCertCollection to trustCertCollection
         */
        public void setTrustCertCollection(String trustCertCollection) {
            this.trustCertCollection = trustCertCollection;

            checkRep();
        }

        /**
         * @requires clientCertChain not null && RESOURCE(clientCertChain)
         * @modifies this
         * @effects Sets this.clientCertChain to clientCertChain
         */
        public void setClientCertChain(String clientCertChain) {
            this.clientCertChain = clientCertChain;

            checkRep();
        }

        /**
         * @requires clientPrivateKey not null && RESOURCE(clientPrivateKey)
         * @modifies this
         * @effects Sets this.clientPrivateKey to clientPrivateKey
         */
        public void setClientPrivateKey(String clientPrivateKey) {
            this.clientPrivateKey = clientPrivateKey;

            checkRep();
        }

        ////////////////////
        // HELPER METHODS
        ////////////////////

        private static String classpathOf(String fileName) {
            return String.format("classpath:sslcert/%s", fileName);
        }
    } // end ChannelConfiguration
}
