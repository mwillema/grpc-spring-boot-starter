package com.marcowillemart.grpc.client;

import com.marcowillemart.common.util.Assert;
import com.marcowillemart.common.util.FailureException;
import com.marcowillemart.common.util.Resources;
import com.marcowillemart.grpc.client.GrpcClientProperties.ChannelConfiguration;
import io.grpc.Channel;
import io.grpc.netty.GrpcSslContexts;
import io.grpc.netty.NettyChannelBuilder;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import java.io.File;
import javax.net.ssl.SSLException;
import org.springframework.core.io.ResourceLoader;

/**
 * Factory that creates Netty gRPC channels.
 *
 * @author Marco Willemart
 */
public class NettyGrpcChannelFactory implements GrpcChannelFactory {

    private final ResourceLoader resourceLoader;
    private final GrpcClientProperties properties;

    public NettyGrpcChannelFactory(
            ResourceLoader resourceLoader,
            GrpcClientProperties properties) {

        Assert.notNull(resourceLoader);
        Assert.notNull(properties);

        this.resourceLoader = resourceLoader;
        this.properties = properties;
    }

    @Override
    public Channel createChannel(String name) {
        Assert.isTrue(properties.getChannels().containsKey(name));

        ChannelConfiguration channel = properties.getChannels().get(name);

        return NettyChannelBuilder
                .forAddress(channel.getHost(), channel.getPort())
                .sslContext(clientAuthentication(channel))
                .build();
    }

    ////////////////////
    // HELPER METHODS
    ////////////////////

    private SslContext clientAuthentication(ChannelConfiguration channel) {
        File trustCertCollectionFile = toFile(channel.getTrustCertCollection());
        File clientCertChainFile = toFile(channel.getClientCertChain());
        File clientPrivateKeyFile = toFile(channel.getClientPrivateKey());

        SslContextBuilder sslContextBuilder =
                GrpcSslContexts.forClient()
                        .trustManager(trustCertCollectionFile)
                        .keyManager(clientCertChainFile, clientPrivateKeyFile);

        try {
            return sslContextBuilder.build();
        } catch (SSLException ex) {
            throw new FailureException(ex);
        }
    }

    private File toFile(String resourceLocation) {
        return Resources.toFile(resourceLoader.getResource(resourceLocation));
    }
}
