package com.marcowillemart.grpc.client;

import io.grpc.Channel;

/**
 * Factory responsible for creating gRPC channels.
 *
 * @author Marco Willemart
 */
public interface GrpcChannelFactory {

    /**
     * @requires there exists a channel configuration named 'name'
     * @return the created channel named 'name'
     */
    Channel createChannel(String name);
}
