package com.marcowillemart.grpc.client;

import io.grpc.Channel;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ResourceLoader;

/**
 * AutoConfiguration for gRPC client.
 *
 * @author Marco Willemart
 */
@Configuration
@EnableConfigurationProperties(GrpcClientProperties.class)
@ConditionalOnClass(Channel.class)
public class GrpcClientAutoConfiguration {

    public GrpcClientAutoConfiguration() {
    }

    @Bean
    @ConditionalOnMissingBean
    public GrpcChannelFactory grpcChannelFactory(
            ResourceLoader resourceLoader,
            GrpcClientProperties properties) {

        return new NettyGrpcChannelFactory(resourceLoader, properties);
    }
}
