package com.marcowillemart.grpc.client;

import static org.assertj.core.api.Assertions.*;
import org.junit.Test;
import org.springframework.boot.autoconfigure.AutoConfigurations;
import org.springframework.boot.test.context.runner.ApplicationContextRunner;

public class GrpcClientAutoConfigurationTest {

    private final ApplicationContextRunner contextRunner =
            new ApplicationContextRunner()
                    .withConfiguration(
                            AutoConfigurations.of(
                                    GrpcClientAutoConfiguration.class));

    @Test
    public void testDefaultAutoConfiguration() {
        contextRunner.run(context -> {
            assertThat(context).hasSingleBean(GrpcClientProperties.class);

            assertThat(
                    context.getBean(GrpcClientProperties.class).getChannels())
                    .containsKey(GrpcClientProperties.DEFAULT_CHANNEL);

            assertThat(context).hasSingleBean(GrpcChannelFactory.class);

            assertThat(
                    context.getBean(GrpcChannelFactory.class)
                            .createChannel(
                                    GrpcClientProperties.DEFAULT_CHANNEL))
                    .isNotNull();
        });
    }

    @Test
    public void testCustomPortOfDefaultChannel() {
        contextRunner
                .withPropertyValues("grpc.client.channels.default.port=1234")
                .run(context -> {
                    assertThat(
                            context.getBean(GrpcClientProperties.class)
                                    .getChannels()
                                    .get("default")
                                    .getPort())
                            .isEqualTo(1234);

                    assertThat(context.getBean(GrpcChannelFactory.class)
                            .createChannel("default")).isNotNull();
                });
    }

    @Test
    public void testCustomChannel() {
        contextRunner
                .withPropertyValues(
                        "grpc.client.channels.eventstore.host=cloud.google.com",
                        "grpc.client.channels.eventstore.port=1234")
                .run(context -> {
                    assertThat(
                            context.getBean(GrpcClientProperties.class)
                                    .getChannels()
                                    .get("eventstore")
                                    .getHost())
                            .isEqualTo("cloud.google.com");

                    assertThat(
                            context.getBean(GrpcClientProperties.class)
                                    .getChannels()
                                    .get("eventstore")
                                    .getPort())
                            .isEqualTo(1234);

                    assertThat(context.getBean(GrpcChannelFactory.class)
                            .createChannel("eventstore")).isNotNull();
                });
    }
}
